/*
 * pwmin.h
 *
 *  Created on: 2015/08/11
 *      Author: �@��Y
 */

#ifndef PWMIN_H_
#define PWMIN_H_
#include "LPC8xx.h"

void pwmin_init(int input_pin,int period);

unsigned int pwmin_get(int ch);

int pwmin_check_timeout(int ch);




#endif /* PWMIN_H_ */
