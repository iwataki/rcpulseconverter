#ifndef _UART_DEF
#define _UART_DEF


#define	MAINCLK		60000000	/* Main clock frequency */
#define	UARTCLK		(115200*16)	/* PCLK_UART frequency generated by fractional divider */

#define USE_UART0	1		/* Use UART0 function */
#define UART0_BPS 	115200	/* UART0 bit rate */
#define UART0_TXB	64		/* Size of Tx buffer (0:polling) */
#define UART0_RXB	64		/* Size of Rx buffer (0:polling) */

#define USE_UART1	0		/* Use UART1 function */
#define UART1_BPS 	38400
#define UART1_TXB	64
#define UART1_RXB	64

#define USE_UART2	0		/* Use UART2 function (for only LPC812M101JDH16, LPC812M101JDH20 and LPC812M101JTB16) */
#define UART2_BPS 	38400
#define UART2_TXB	64
#define UART2_RXB	64


#include "LPC8xx.h"

void uartclk_init (void);

#if USE_UART0
void uart0_init (void);
int uart0_test (void);
void uart0_putc (uint8_t);
uint8_t uart0_getc (void);
#endif
#if USE_UART1
void uart1_init (void);
int uart1_test (void);
void uart1_putc (uint8_t);
uint8_t uart1_getc (void);
#endif
#if USE_UART2
void uart2_init (void);
int uart2_test (void);
void uart2_putc (uint8_t);
uint8_t uart2_getc (void);
#endif

#endif
