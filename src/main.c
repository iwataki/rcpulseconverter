//**********************
// Barometer and Temperature for LP810-DEMO board
//
// (C)Copyright 2013 All rights reserved by Y.Onodera
// http://einstlab.web.fc2.com
//**********************
#include <stdio.h>
#include <string.h>
#include "LPC8xx.h"
#include "uart81x.h"
#include "xprintf.h"
#include "pwmin.h"
#include "mrt.h"

#if defined(__CODE_RED)
//  #include <cr_section_macros.h>
//  #include <NXP/crp.h>
//  __CRP const unsigned int CRP_WORD = CRP_NO_CRP ;
#endif

void SwitchMatrix_Init();
void IOCON_Init();

void loop(void);

int main(void)
{
	SwitchMatrix_Init();
	//LPC_SYSCON->SYSAHBCLKCTRL|=(1<<6);
	//LPC_GPIO_PORT->DIR0|=(1<<2)|(1<<3);
	IOCON_Init();
	uartclk_init();
	uart0_init();
	xdev_in(uart0_getc);
	xdev_out(uart0_putc);
	xprintf("Hello LPC810\n");
	//mrtCyclicOperationInit(10000,loop);
	pwmin_init(0,14400);
	pwmin_init(1,14400);
	while(1){
		char c=uart0_getc();
		if(c=='#'){
			xprintf("%d,%d\n",pwmin_get(0),pwmin_get(1));
		}
		//LPC_GPIO_PORT->NOT0=(1<<3)|(1<<2);
	}

}
void loop(void){
//	static int loopcnt=0;
//
//	if(loopcnt>10){
//		xprintf("Val=");
//		int i;
//		for(i=0;i<2;i++){
//			if(pwmin_check_timeout(i)){
//				xprintf("No signal,");
//			}else{
//				xprintf("%5d,",pwmin_get(i));
//			}
//		}
//		xprintf("\n\r");
//		loopcnt=0;
//	}else{
//		loopcnt++;
//	}

}


