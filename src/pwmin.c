/*
 * pwmin.c
 *
 *  Created on: 2015/08/11
 *      Author: �@��Y
 */

#include "pwmin.h"
#include "xprintf.h"
unsigned int rising_edge_countval[4];
unsigned int measured_time[4];
unsigned int signalfail[4];
unsigned int baseperiod;
void pwmin_init(int input_pin,int period){
	baseperiod=period;
	static int timebase_initialized=0;
	unsigned int eventctrl=0;
	if(timebase_initialized==0){
		timebase_initialized=1;
		LPC_SYSCON->SYSAHBCLKCTRL|=1<<8;//SCT ON
		LPC_SCT->CONFIG|=0x01|1<<17;//32bit timer, match0 as limit
		unsigned int prescale=period*(SystemCoreClock/1000000)/0xfff-1;
		LPC_SCT->CTRL_U|=prescale<<5|1<<3;//set prescale value and reset counter
		LPC_SCT->MATCH[0].U=0xfff;
		LPC_SCT->MATCHREL[0].U=0xfff;
		LPC_SCT->EVENT[0].STATE|=0x03;//event both state 0 and state 1
		eventctrl=1<<12|1<<15|1<<14;
		LPC_SCT->EVENT[0].CTRL=eventctrl;//On event0(match0),goto state 1
		LPC_SCT->EVEN|=1;//event 0 interrupt eneble
		NVIC_EnableIRQ(SCT_IRQn);
	}
	LPC_SCT->CTRL_U|=(1<<2);//halt counter
	LPC_SCT->REGMODE_L|=1<<(input_pin+1);// match register (input_pin+1) is capture
	LPC_SCT->EVEN|=1<<(input_pin+1);//event (input_pin+1) enable
	LPC_SCT->CAPCTRL[input_pin+1].U|=1<<(input_pin+1);//capture when event input_pin+1
	LPC_SCT->EVENT[input_pin+1].STATE|=0x03;
	eventctrl=0;
	eventctrl=1<<14|2<<12|1<<10|input_pin<<6;//On event,goto state 0, io only, rising, CTin=inputpin
	LPC_SCT->EVENT[input_pin+1].CTRL=eventctrl;
	LPC_SCT->CTRL_U&=~(1<<2);//Start counter;
}

unsigned int pwmin_get(int ch){
	return (measured_time[ch]&0xfff)*baseperiod/0xfff;
}
int pwmin_check_timeout(int ch){
	return signalfail[ch];
}
void SCT_IRQHandler(void){
	unsigned int flag=LPC_SCT->EVFLAG;
	static int timeoutcount[4]={0,0,0,0};
//	if(flag!=0x01){
//		xprintf("flag=%x\n\r",flag);
//	}
	if(flag&(1<<0)){//overflow
		LPC_SCT->EVFLAG=1<<0;
		int i;
		for(i=0;i<4;i++){
			timeoutcount[i]++;
			if(timeoutcount[i]>1){//no signal
				signalfail[i]=1;
			}
		}
	}
	int i;
	for(i=0;i<4;i++){
		if(flag&(1<<(i+1))){//ctin(n),event(1+n)
			LPC_SCT->EVFLAG=1<<(i+1);
			timeoutcount[i]=0;
			signalfail[i]=0;
			unsigned int temp=LPC_SCT->EVENT[i+1].CTRL;
			if((temp&(3<<10))==(1<<10)){//rising edge
				rising_edge_countval[i]=LPC_SCT->CAP[i+1].U;
				temp&=~(3<<10);
				temp|=(2<<10);//set falling edge
				//xprintf("Rising %d\n\r",i);
			}else{//falling edge
				measured_time[i]=LPC_SCT->CAP[i+1].U-rising_edge_countval[i];
				temp&=~(3<<10);
				temp|=(1<<10);//set rising edge
				//xprintf("Falling %d\n\r",i);
			}
			LPC_SCT->EVENT[i+1].CTRL=temp;
		}
	}

}
