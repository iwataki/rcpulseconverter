/*------------------------------------------------------------------------/
/  LPC81x USART basic control module
/-------------------------------------------------------------------------/
/
/  Copyright (C) 2013, ChaN, all right reserved.
/
/ * This software is a free software and there is NO WARRANTY.
/ * No restriction on use. You can use, modify and redistribute it for
/   personal, non-profit or commercial products UNDER YOUR RESPONSIBILITY.
/ * Redistributions of source code must retain the above copyright notice.
/
/-------------------------------------------------------------------------*/

#include "uart81x.h"


void uartclk_init (void)
{
	/* Configure USART fractional divider */
	LPC_SYSCON->UARTFRGDIV=255;
	LPC_SYSCON->UARTFRGMULT=(((double)MAINCLK / UARTCLK) / (MAINCLK / UARTCLK) - 1) * 256;
	/* Configure USART main divider */
	LPC_SYSCON->UARTCLKDIV = MAINCLK / UARTCLK;
}


#if USE_UART0
#if UART0_TXB
static volatile struct {
	uint16_t	ri, wi, ct;
	uint8_t		buff[UART0_TXB];
} TxBuff0;
#endif
#if UART0_RXB
static volatile struct {
	uint16_t	ri, wi, ct;
	uint8_t		buff[UART0_RXB];
} RxBuff0;
#endif


#if UART0_TXB || UART0_RXB
void UART0_IRQHandler (void)
{
	uint16_t stat;
	uint8_t d;
	int i, cnt;


	stat = LPC_USART0->STAT;
#if UART0_RXB
	if (stat & 0x0001) {
		d = LPC_USART0->RXDATA;
		cnt = RxBuff0.ct;
		if (cnt < UART0_RXB) {	/* Store data if Rx buffer is not full */
			i = RxBuff0.wi;
			RxBuff0.buff[i++] = d;
			RxBuff0.wi = i % UART0_RXB;
			RxBuff0.ct = ++cnt;
		}
	}
#endif
#if UART0_TXB
	if (stat & 0x004) {
		cnt = TxBuff0.ct;
		if (cnt) {		/* There is one or more byte to send */
			TxBuff0.ct = --cnt;
			i = TxBuff0.ri;
			d = TxBuff0.buff[i++];
			LPC_USART0->TXDATA= d;
			TxBuff0.ri = i % UART0_TXB;
		} else {
			LPC_USART0->INTENCLR= 0x0004;	/* Disable Tx interrupt */
		}
	}
#endif
}
#endif


int uart0_test (void)
{
#if UART0_RXB
	return RxBuff0.ct;
#else
	return (int)((U0STAT & 0x0001) != 0);
#endif
}


uint8_t uart0_getc (void)
{
		uint8_t d;

#if UART0_RXB
	/* Wait while Rx buffer is empty */
	while (!RxBuff0.ct) ;
	{
		int i;

		i = RxBuff0.ri;			/* Get a byte from Rx buffer */
		d = RxBuff0.buff[i++];
		RxBuff0.ri = i % UART0_RXB;
		__disable_irq();
		RxBuff0.ct--;
		__enable_irq();
	}
#else
	while (!(U0STAT & 0x0001)) ;
	d = U0RXDAT;
#endif

	return d;
}


void uart0_putc (uint8_t d)
{
#if UART0_TXB
	/* Wait for Tx buffer ready */
	while (TxBuff0.ct >= UART0_TXB) ;
	{
		int i;
		i = TxBuff0.wi;		/* Put a byte into Tx byffer */
		TxBuff0.buff[i++] = d;
		TxBuff0.wi = i % UART0_TXB;
		__disable_irq();
		TxBuff0.ct++;
		LPC_USART0->INTENSET= 0x0004;
		__enable_irq();
	}
#else
	while (!(U0STAT & 0x0004)) ;
	U0TXDAT = d;
#endif
}


void uart0_init (void)
{
	NVIC_DisableIRQ(UART0_IRQn);
	/* Enable UART0 module */

	LPC_SYSCON->SYSAHBCLKCTRL|=1<<14;
	/* Initialize UART */
	LPC_USART0->BRG = UARTCLK / (UART0_BPS * 16) - 1;	/* Bit rate */
	LPC_USART0->CFG= 0x0005;	/* Format: N81 */

	/* Clear Tx/Rx buffers */
#if UART0_TXB
	TxBuff0.ri = 0; TxBuff0.wi = 0; TxBuff0.ct = 0;
#endif
#if UART0_RXB
	RxBuff0.ri = 0; RxBuff0.wi = 0; RxBuff0.ct = 0;
	LPC_USART0->INTENSET=0x0001;
#endif
#if UART0_TXB || UART0_RXB
	NVIC_EnableIRQ(UART0_IRQn);
#endif
}

#endif	/* USE_UART0 */


#if USE_UART1

#if UART1_TXB
static volatile struct {
	uint16_t	ri, wi, ct;
	uint8_t		buff[UART1_TXB];
} TxBuff1;
#endif
#if UART1_RXB
static volatile struct {
	uint16_t	ri, wi, ct;
	uint8_t		buff[UART1_RXB];
} RxBuff1;
#endif


#if UART1_TXB || UART1_RXB
void UART1_IRQHandler (void)
{
	uint16_t stat;
	uint8_t d;
	int i, cnt;


	stat = U1STAT;
#if UART1_RXB
	if (stat & 0x0001) {
		d = U1RXDAT;
		cnt = RxBuff1.ct;
		if (cnt < UART1_RXB) {	/* Store data if Rx buffer is not full */
			i = RxBuff1.wi;
			RxBuff1.buff[i++] = d;
			RxBuff1.wi = i % UART1_RXB;
			RxBuff1.ct = ++cnt;
		}
	}
#endif
#if UART1_TXB
	if (stat & 0x0004) {
		cnt = TxBuff1.ct;
		if (cnt) {		/* There is one or more byte to send */
			TxBuff1.ct = --cnt;
			i = TxBuff1.ri;
			d = TxBuff1.buff[i++];
			U1TXDAT = d;
			TxBuff1.ri = i % UART1_TXB;
		} else {
			U1INTENCLR = 0x0004;	/* Disable Tx interrupt */
		}
	}
#endif
}
#endif


int uart1_test (void)
{
#if UART1_RXB
	return RxBuff1.ct;
#else
	return (int)((U1STAT & 0x0001) != 0);
#endif
}


uint8_t uart1_getc (void)
{
		uint8_t d;

#if UART1_RXB
	/* Wait while Rx buffer is empty */
	while (!RxBuff1.ct) ;
	{
		int i;

		i = RxBuff1.ri;			/* Get a byte from Rx buffer */
		d = RxBuff1.buff[i++];
		RxBuff1.ri = i % UART1_RXB;
		__disable_irq();
		RxBuff1.ct--;
		__enable_irq();
	}
#else
	while (!(U1STAT & 0x0001)) ;
	d = U1RXDAT;
#endif

	return d;
}


void uart1_putc (uint8_t d)
{
#if UART1_TXB
	/* Wait for Tx buffer ready */
	while (TxBuff1.ct >= UART1_TXB) ;
	{
		int i;
		i = TxBuff1.wi;		/* Put a byte into Tx byffer */
		TxBuff1.buff[i++] = d;
		TxBuff1.wi = i % UART1_TXB;
		__disable_irq();
		TxBuff1.ct++;
		U1INTENSET = 0x0004;
		__enable_irq();
	}
#else
	while (!(U1STAT & 0x0004)) ;
	U1TXDAT = d;
#endif
}


void uart1_init (void)
{
	__disable_irqn(UART1_IRQn);

	/* Enable UART1 module */
	__enable_ahbclk(PCUART1);

	/* Initialize UART */
	U1BRG = UARTCLK / (UART1_BPS * 16) - 1;	/* Bit rate */
	U1CFG = 0x0005;	/* Format: N81 */

	/* Clear Tx/Rx buffers */
#if UART1_TXB
	TxBuff1.ri = 0; TxBuff1.wi = 0; TxBuff1.ct = 0;
#endif
#if UART1_RXB
	RxBuff1.ri = 0; RxBuff1.wi = 0; RxBuff1.ct = 0;
	U1INTENSET = 0x0001;
#endif
#if UART1_TXB || UART1_RXB
	__enable_irqn(UART1_IRQn);
#endif
}

#endif	/* USE_UART1 */


#if USE_UART2

#if UART2_TXB
static volatile struct {
	uint16_t	ri, wi, ct;
	uint8_t		buff[UART2_TXB];
} TxBuff2;
#endif
#if UART2_RXB
static volatile struct {
	uint16_t	ri, wi, ct;
	uint8_t		buff[UART2_RXB];
} RxBuff2;
#endif


#if UART2_TXB || UART2_RXB
void UART2_IRQHandler (void)
{
	uint16_t stat;
	uint8_t d;
	int i, cnt;


	stat = U2STAT;
#if UART2_RXB
	if (stat & 0x0001) {
		d = U2RXDAT;
		cnt = RxBuff2.ct;
		if (cnt < UART2_RXB) {	/* Store data if Rx buffer is not full */
			i = RxBuff2.wi;
			RxBuff2.buff[i++] = d;
			RxBuff2.wi = i % UART2_RXB;
			RxBuff2.ct = ++cnt;
		}
	}
#endif
#if UART2_TXB
	if (stat & 0x0004) {
		cnt = TxBuff2.ct;
		if (cnt) {		/* There is one or more byte to send */
			TxBuff2.ct = --cnt;
			i = TxBuff2.ri;
			d = TxBuff2.buff[i++];
			U2TXDAT = d;
			TxBuff2.ri = i % UART2_TXB;
		} else {
			U2INTENCLR = 0x0004;	/* Disable Tx interrupt */
		}
	}
#endif
}
#endif


int uart2_test (void)
{
#if UART2_RXB
	return RxBuff2.ct;
#else
	return (int)((U2STAT & 0x0001) != 0);
#endif
}


uint8_t uart2_getc (void)
{
		uint8_t d;

#if UART2_RXB
	/* Wait while Rx buffer is empty */
	while (!RxBuff2.ct) ;
	{
		int i;

		i = RxBuff2.ri;			/* Get a byte from Rx buffer */
		d = RxBuff2.buff[i++];
		RxBuff2.ri = i % UART2_RXB;
		__disable_irq();
		RxBuff2.ct--;
		__enable_irq();
	}
#else
	while (!(U2STAT & 0x0001)) ;
	d = U2RXDAT;
#endif

	return d;
}


void uart2_putc (uint8_t d)
{
#if UART2_TXB
	/* Wait for Tx buffer ready */
	while (TxBuff2.ct >= UART2_TXB) ;
	{
		int i;
		i = TxBuff2.wi;		/* Put a byte into Tx byffer */
		TxBuff2.buff[i++] = d;
		TxBuff2.wi = i % UART2_TXB;
		__disable_irq();
		TxBuff2.ct++;
		U2INTENSET = 0x0004;
		__enable_irq();
	}
#else
	while (!(U2STAT & 0x0004)) ;
	U2TXDAT = d;
#endif
}


void uart2_init (void)
{
	__disable_irqn(UART2_IRQn);

	/* Enable UART2 module */
	__enable_ahbclk(PCUART2);

	/* Initialize UART */
	U2BRG = UARTCLK / (UART2_BPS * 16) - 1;	/* Bit rate */
	U2CFG = 0x0005;	/* Format: N81 */

	/* Clear Tx/Rx buffers */
#if UART2_TXB
	TxBuff2.ri = 0; TxBuff2.wi = 0; TxBuff2.ct = 0;
#endif
#if UART2_RXB
	RxBuff2.ri = 0; RxBuff2.wi = 0; RxBuff2.ct = 0;
	U2INTENSET = 0x0001;
#endif
#if UART2_TXB || UART2_RXB
	__enable_irqn(UART2_IRQn);
#endif
}

#endif	/* USE_UART2 */

